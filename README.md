The list of jupyter notebooks for RTA Workshop:

- DataGenerator

- KerasCNN1D

- GWExample


Design sensitivity curve was created for Virgo Interferometer.

Required Python packages:

- numpy

- scipy

- matplotlib

- sklearn

- tensorflow

- keras

- statsmodel

- h5py

